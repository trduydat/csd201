
import java.util.Scanner;

/**
 * To prompt user to get their input while check its validity
 *
 * @author trduy
 */
/**
 * TODO: [] Add support for checking range of prime data type range
 *
 */
public class GetInput {

    static Scanner getStdInput = new Scanner(System.in);
    static final String ERROR_MSG_SHORT = "\nInvalid input. Please enter a short (integer in range of -32,768 to 32,767)!\n";
    static final String ERROR_MSG_INT = "\nInvalid input. Please enter an integer value!\n";
    static final String ERROR_MSG_FLOAT = "\nInvalid input. Please enter a float value!\n";
    static final String ERROR_MSG_DOUBLE = "\nInvalid input. Please enter a double value!\n";

    public static short Short(final String prefixMsg) {
        short shortVal = 0;

        while (true) {
            try {
                System.out.print(prefixMsg + ": ");
                shortVal = Short.parseShort(getStdInput.nextLine());
            } catch (final NumberFormatException ex) {
                System.err.println(ERROR_MSG_SHORT + ex.getMessage() + "\n");
                continue;
            }
            break;
        }

        return shortVal;
    }

    public static short Short(final String prefixMsg, short min) {
        short shortVal = 0;

        while (true) {
            try {
                System.out.print(prefixMsg + ": ");
                shortVal = Short.parseShort(getStdInput.nextLine());
            } catch (final NumberFormatException ex) {
                System.err.println(ERROR_MSG_SHORT + ex.getMessage() + "\n" );
                continue;
            }

            if (shortVal < min) {
                System.err.format("\nInvalid input. Your input should be larger than %d. Please try again!\n", min);
            } else {
                break;
            }
        }

        return shortVal;
    }

    public static short Short(String prefixMsg, short min, short max) {
        short shortVal = 0;

        while (true) {
            try {
                System.out.print(prefixMsg + ": ");
                shortVal = Short.parseShort(getStdInput.nextLine());
            } catch (final NumberFormatException ex) {
                System.err.println(ERROR_MSG_SHORT + ex.getMessage() + "\n");
                continue;
            }

            if (shortVal < min || shortVal > max) {
                System.err.format("\nInvalid input. Your input value should between %d and %d. Please try again!\n", min, max);
            } else {
                break;
            }
        }

        return shortVal;
    }

    public static int Integer(String prefixMsg) {
        int intVal = 0;

        while (true) {
            try {
                System.out.print(prefixMsg + ": ");
                intVal = Integer.parseInt(getStdInput.nextLine());
            } catch (final NumberFormatException ex) {
                System.err.println(ERROR_MSG_INT + ex.getMessage() + "\n");
                continue;
            }
            break;
        }

        return intVal;
    }

    public static int Integer(String prefixMsg, int min, int max) {
        int intVal = 0;

        while (true) {
            try {
                System.out.print(prefixMsg + ": ");
                intVal = Integer.parseInt(getStdInput.nextLine());
            } catch (final NumberFormatException ex) {
                System.err.println(ERROR_MSG_INT + ex.getMessage() + "\n");
                continue;
            }

            if (intVal < min || intVal > max) {
                System.err.format(
                        "\nInvalid input. Your input should be in valid range from %d to %d. Please try again!\n\n",
                        min, max);
            } else {
                break;
            }
        }

        return intVal;
    }

    public static double Double(String prefixMsg) {
        double intVal = 0;

        while (true) {
            try {
                System.out.print(prefixMsg + ": ");
                intVal = Double.parseDouble(getStdInput.nextLine());
            } catch (final NumberFormatException ex) {
                System.err.println(ERROR_MSG_DOUBLE + ex.getMessage() + "\n");
                continue;
            }
            break;
        }

        return intVal;
    }

    public static double Double(String prefixMsg, double min, double max) {
        double doubleVal = 0;

        while (true) {
            try {
                System.out.print(prefixMsg + ": ");
                doubleVal = Double.parseDouble(getStdInput.nextLine());
            } catch (final NumberFormatException ex) {
                System.err.println(ERROR_MSG_DOUBLE + ex.getMessage() + "\n");
                continue;
            }

            if (doubleVal < min || doubleVal > max) {
                System.err.format(
                        "\nInvalid input. Your input should be in valid range from %.2f to %.2f Please try again!\n",
                        min, max);
            } else {
                break;
            }
        }

        return doubleVal;
    }

    public static double Double(String prefixMsg, double min) {
        double doubleVal = 0;

        while (true) {
            try {
                System.out.print(prefixMsg + ": ");
                doubleVal = Double.parseDouble(getStdInput.nextLine());
            } catch (final NumberFormatException ex) {
                System.err.println(ERROR_MSG_DOUBLE + ex.getMessage() + "\n");
                continue;
            }

            if (doubleVal < min) {
                System.err.format("\nInvalid input. Your input should be larger than %.2f\n", min);
            } else {
                break;
            }
        }

        return doubleVal;
    }

    public static float Float(String prefixMsg, float min) {
        float floatVal = 0f;

        while (true) {
            try {
                System.out.print(prefixMsg + ": ");
                floatVal = Float.parseFloat(getStdInput.nextLine());
            } catch (final NumberFormatException ex) {
                System.err.println(ERROR_MSG_DOUBLE + ex.getMessage() + "\n");
                continue;
            }

            if (floatVal < min) {
                System.err.format("\nInvalid input. Your input should be larger than %.2f\n", min);
            } else {
                break;
            }
        }

        return floatVal;
    }

    public static float Float(String prefixMsg, float min, float max) {
        float floatVal = 0f;

        while (true) {
            try {
                System.out.print(prefixMsg + ": ");
                floatVal = Float.parseFloat(getStdInput.nextLine());
            } catch (final NumberFormatException ex) {
                System.err.println(ERROR_MSG_DOUBLE + ex.getMessage() + "\n");
                continue;
            }

            if (floatVal < min || floatVal > max) {
                System.err.format("\nInvalid input. Your input value should between %.2f and %.2f\n", min, max);
            } else {
                break;
            }
        }

        return floatVal;
    }

    public static String String(String prefixMsg) {
        String stringValue;

        System.out.print(prefixMsg + ": ");
        stringValue = getStdInput.nextLine();

        return stringValue;
    }
}
