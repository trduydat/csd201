
/**
 *
 * @author trdat
 */
public class Main {

    public static void main(String[] args) {
        short userChoice;
        
        // Get user choice
        do {
            System.out.println("1. Calculate perimeter and area of a rectangle");
            System.out.println("2. Calculate perimeter and area of a triangle");
            System.out.println("3. Calculate perimeter and area of a circle");
            userChoice = GetInput.Short("Enter your choice (0 for exit)", (short) 0, (short) 3);

            switch (userChoice) {
                case 1: {
                    // Calculate perimeter and area of a rectangle
                    float a = GetInput.Float("Enter width", 0);
                    float b = GetInput.Float("Enter length", 0);
                    Rectangle rec = new Rectangle(a, b);
                    rec.display();
                    break;
                }
                case 2: {
                    // Calculate perimeter and area of a triangle
                    float a = GetInput.Float("Enter edge a", 0);
                    float b = GetInput.Float("Enter edge b", 0);
                    float c = GetInput.Float("Enter edge c", 0);
                    Triangle tri = new Triangle(a, b, c);
                    tri.display();
                    break;
                }
                case 3: {
                    // Calculate perimeter and area of a circle
                    float r = GetInput.Float("Enter radius", 0);
                    Circle cir = new Circle(r);
                    cir.display();
                    break;
                }
                case 0: {
                    // Exit program
                    System.out.println("\nThanks for using my program");
                    System.exit(0);
                }
                default:
                    break;
            }
            System.out.println();
        } while (userChoice != 0);
    }
}
