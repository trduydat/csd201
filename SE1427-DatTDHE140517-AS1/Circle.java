
/**
 *
 * @author trdat
 */
public class Circle {

    float radius;

    public Circle() {
    }

    public Circle(float radius) {
        this.radius = radius;
    }

    public float getRadius() {
        return radius;
    }

    public void setRadius(float radius) {
        this.radius = radius;
    }

    public float computePerimeter() {
        return (float) (this.radius * 2 * Math.PI);
    }

    public float computeArea() {
        return (float) (Math.pow(this.radius, 2) * Math.PI);
    }

    public void display() {
        System.out.format("The perimeter of the circle: %.2f\n", computePerimeter());
        System.out.format("The area of the circle: %.2f\n", computeArea());
    }
}
