
/**
 *
 * @author trdat
 */
public class Rectangle {

    float width;
    float length;

    public Rectangle() {
    }

    public Rectangle(float width, float length) {
        this.width = width;
        this.length = length;
    }

    public float getWidth() {
        return width;
    }

    public float getLength() {
        return length;
    }

    public void setWidth(float width) {
        this.width = width;
    }

    public void setLength(float length) {
        this.length = length;
    }

    public float computePremeter() {
        return (float) (2 * (this.width + this.length));
    }

    public float computeArea() {
        return (float) (this.width * this.length);
    }

    public void display() {
        System.out.format("The perimeter of the rectangle: %.2f\n", computePremeter());
        System.out.format("The area of the rectangle: %.2f\n", computeArea());
    }
}
