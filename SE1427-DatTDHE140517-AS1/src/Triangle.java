
/**
 *
 * @author trdat
 */
public class Triangle {

    float a;
    float b;
    float c;

    public Triangle() {
    }

    public Triangle(float a, float b, float c) {
        this.a = a;
        this.b = b;
        this.c = c;
    }

    public boolean isTriangle() {
        return this.a + this.b > this.c && this.b + this.c > this.a && this.a + this.c > this.b;
    }

    public float computePerimeter() {
        if (isTriangle()) {
            return this.a + this.b + this.c;
        } else {
            return 0;
        }
    }

    public float computeArea() {
        if (isTriangle()) {
            float s;
            s = (float) (0.5 * computePerimeter());
            return (float) (Math.sqrt(s * (s - this.a) * (s - this.b) * (this.c)));
        } else {
            return 0;
        }
    }

    public void display() {
        if (isTriangle()) {
            System.out.println("This is a triangle");
            System.out.format("The perimeter of the triangle: %.2f\n", computePerimeter());
            System.out.format("The area of the triangle: %.2f\n", computeArea());
        } else {
            System.out.println("This is not a triangle");
        }
    }
}
