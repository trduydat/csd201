/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author trdat
 */
public class MyLinkedList {

    Node head, tail;

    public MyLinkedList() {
        head = null;
        tail = null;
    }

    public boolean isEmpty() {
        return head == null;
    }

    public boolean isLast() {
        return head.next == tail;
    }

    public int size() {
        int count = 0;
        Node node = head;

        while (node != null) {
            count++;
            node = node.next;
        }

        return count;
    }

    public boolean lookup(int data) {
        Node temp = head;
        boolean isFound = false;

        if (head.data == data) {
            isFound = true;
        } else {
            while (temp.next != null) {
                if (temp.data == data) {
                    isFound = true;
                    break;
                } else {
                    temp = temp.next;
                }
            }
        }

        return isFound;

    }

    /**
     * Get and return Node in x position
     *
     * @param position
     * @return node
     */
    public Node get(int position) {
        Node node = head;
        int count = 0;

        while (node != null && count < position) {
            count++;
            node = node.next;
        }

        return node;
    }

    /**
     * Add a node with value x at the tail of a list
     *
     * @param data
     */
    public void add(int data) {
        Node newNode = new Node(data);
        if (isEmpty()) {
            head = newNode;
            tail = newNode;
        } else {
            tail.next = newNode;
            tail = newNode;
        }
    }

    /**
     * Traverse from head to tail and display info of all nodes in the list
     */
    public void traverse() {
        Node node = head;
        while (node != null) {
            System.out.print(node.data);
            System.out.format("%s", node.next == null ? "" : ", ");
            node = node.next;
        }
        System.out.println();
    }

    public void delete(int data) {
        boolean isDeleted = false;

        if (head.data == data) {
            head = head.next;
        } else {
            Node temp = head;
            while (temp.next != null) {
                if (temp.next.data == data) {
                    temp.next = temp.next.next;
                    isDeleted = true;
                    break;
                } else {
                    temp = temp.next;
                }
            }
        }

        if (isDeleted) {
            System.out.println("deleted " + data + ": ");
        } else {
            System.out.println("node not found!");
        }
    }

    /**
     * Search and return the reference to the first node having info x
     *
     * @param data
     * @return node
     */
    public Node search(int data) {
        Node node = head;
        int index = 0;

        while (node != null) {
            if (node.data == data) {
                switch (index) {
                    case 1:
                        System.out.println("Found at the 1-st position.");
                        break;
                    case 2:
                        System.out.println("Found at the 2-nd position.");
                        break;
                    case 3:
                        System.out.println("Found at the 3-rd position");
                        break;
                    default:
                        System.out.format("Found at the %d-th position.\n", index);
                        break;
                }

                return node;
            }
            node = node.next;
            index++;

        }

        System.out.println("Not found");
        return null;
    }

    /**
     * count and return number of nodes in the list
     *
     * @return count
     */
    public int count() {
        int count = 0;
        Node node = head;

        while (node != null) {
            count++;
            node = node.next;
        }

        System.out.println("count: " + count);
        return count;
    }

    public void sort() {
        int listSize = size();
        int tmp;
        Node nodeA, nodeB;

        for (int i = 0; i < listSize - 1; i++) {
            for (int j = i + 1; j < listSize; j++) {
                nodeA = get(i);
                nodeB = get(j);
                if (nodeA.data > nodeB.data) {
                    tmp = nodeA.data;
                    nodeA.data = nodeB.data;
                    nodeB.data = tmp;
                }
            }
        }

        this.traverse();
    }

    /**
     * Get and print the maximum value in the LinkedList
     *
     * @return
     */
    public int max() {
        int eMax = 0;
        Node node = head;

        if (head != null) {
            eMax = head.data;
        }

        while (node != null) {
            if (node.data > eMax) {
                eMax = node.data;
            }
            node = node.next;
        }

        System.out.println(eMax + ";");
        return eMax;
    }

    /**
     * Return the sum of all values in the list
     *
     * @return
     */
    public int sum() {
        int sum = 0;

        Node node = head;
        while (node != null) {
            sum += node.data;
            node = node.next;
        }
        System.out.println("sum: " + sum);

        return sum;
    }

    /**
     * Return the average of all values in the list.
     *
     * @return avg value
     */
    public int avg() {
        int sum = 0;

        Node node = head;
        while (node != null) {
            sum += node.data;
            node = node.next;
        }

        int avg = sum / count();
        System.out.println("avg: " + avg);
        return avg;
    }
}
